import { MutationTree } from 'vuex';
import { Election, ElectionState } from '../types';

export const mutations: MutationTree<ElectionState> = {
    electionLoaded(state, payload: Election[]) {
        state.elections = payload;
        state.error = undefined;
    },
    electionError(state, error) {
        state.error = error;
        state.elections = [];
    }
};