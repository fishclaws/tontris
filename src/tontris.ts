import * as PIXI from "pixi.js";

export enum Direction {
  Up,
  Down,
  Left,
  Right
}

export class TontrisSquare {
  constructor(
    public x: number = 4,
    public y: number = 0,
    public color: number
  ) {}


  generateGraphics(blockSize: number) {
    const graphics = new PIXI.Graphics();
    graphics.beginFill(this.color);
    // set the line style to have a width of 5 and set the color to red
    graphics.lineStyle(0.5, 0xFFFFFF);
    // draw a rectangle
    graphics.drawRect(this.x * blockSize, this.y * blockSize, blockSize, blockSize);
    return graphics

  }
}

const configurations = [
  {
    color: 0x0FA043,
    grid: [[0, 0], [0, 1], [1, 0], [1, 1]],
    pivot: [1, 0]
  },
  {
    color: 0x0FA033,
    grid: [[0, 0], [0, 1], [0, 2], [0, 3]],
    pivot: [0, 1]
  },
  {
    color: 0x0FA0FF,
    grid: [[0, 0], [0, 1], [1, 1], [2, 1]],
    pivot: [0, 1]
  },
  {
    color: 0x6FAD43,
    grid: [[0, 0], [-1, 0], [-1, 1], [0, 1]],
    pivot: [-1, 0]
  },
  {
    color: 0x4FA243,
    grid: [[0, 0], [0, 1], [1, 0], [1, 1]],
    pivot: [1, 0]
  }
]

export class TontrisPiece {
  squares: TontrisSquare[]
  pivot: [number, number]

  constructor(
    public x: number,
    public y: number,
    public rotation: number,
    public blockSize: number,
  ) {
    const configIndex = Math.round(Math.random() * configurations.length)
    const config = configurations[configIndex]
    this.squares = config.grid.map(g => new TontrisSquare(g[0], g[1], config.color))
    this.pivot = config.pivot as [number, number]
  }

  generateGraphics() {
    let container = new PIXI.Container();
    for (let square of this.squares) {
      container.addChild(square.generateGraphics(this.blockSize))
    }
    return container
  }

  rotate() {
    const angle = Math.PI / 2
    for (const square of this.squares) {
    //   square.x = this.pivot[0] + this.pivot[1] - square.y
      square.y = this.pivot[1] - this.pivot[0] + square.x
    //   square.x = square.x * Math.cos(angle) - square.y * Math.sin(angle)
    //   square.y = square.x * Math.sin(angle) + square.y * Math.cos(angle)
    }
  }
}


/*

_|_|X|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|

*/

export class Grid {
  data: number[][]
  constructor(
    public width: number,
    public height: number,
    public blockSize: number,
  ) {
    this.data = []
    for (let x = 0; x < width; x++) {
      this.data[x] = []
    }
  }

  generatePiece(): TontrisPiece {
    return new TontrisPiece(
      Math.round(this.width / 2),
      0,
      0,
      this.blockSize)
  }

  set(x: number, y: number, value: number) {
    this.data[x][y] = value
  }

  get(x: number, y: number) {
    return this.data[x][y]
  }

  update(piece: TontrisPiece) {
    for (let square of piece.squares) {
      if (piece.y + square.y == this.height - 1) {
        for (let square of piece.squares)
          this.set(piece.x + square.x, piece.y + square.y, 1)
        return false
      }
      if (this.get(piece.x + square.x, piece.y + square.y + 1)) {
        for (let square of piece.squares)
          this.set(piece.x + square.x, piece.y + square.y, 1)
        return false
      }
    }
    return true
  }
    
  generateGraphics() {
    const container = new PIXI.Container()
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        const graphics = new PIXI.Graphics();
        if (this.get(x, y)) {
          graphics.beginFill(0x000000);
        }
        // set the line style to have a width of 5 and set the color to red
        graphics.lineStyle(1, 0xFF0000);
        // draw a rectangle
        graphics.drawRect(x * this.blockSize, y * this.blockSize, this.blockSize, this.blockSize);
        container.addChild(graphics)
      }
    }
    return container
  }

  allowed(piece: TontrisPiece, direction: Direction) {
    for (const square of piece.squares) {
      if (direction === Direction.Left) {
        if (piece.x + square.x === 0) {
          return false
        } else if (this.get(piece.x + square.x - 1, piece.y + square.y)) {
          return false
        }
      }
      if (direction === Direction.Right) {
        if (piece.x + square.x === this.width - 1) {
          return false
        } else if (this.get(piece.x + square.x + 1, piece.y + square.y)) {
          return false
        }
      }
      if (direction === Direction.Down) {
        if (piece.y + square.y === this.height - 1) {
          return false
        } else if (this.get(piece.x + square.x, piece.y + square.y + 1)) {
          return false
        }
      }
    }
    return true
  }

  checkRows() {
    const clonedData = JSON.parse(JSON.stringify(this.data))
    
    const rowsToClear: number[] = []
    for (let y = 0; y < this.height; y++) {
      for (let x = 0; x < this.width; x++) {
        if (!this.get(x, y)) {
          break;
        }
        if (x === this.width - 1) {
          // row needs to be cleared
          for (let yy = 0; yy < y; yy++) {
            // loop throw all rows above
            for (let xx = 0; xx < this.width; xx++) {
              // loop throw column on each row
              const value = clonedData[xx][yy]
              clonedData[xx][yy+1] = value
            }
          }
        }
      }
    }

    // for (let row of rowsToClear) {
    //   for (let y = 0; y < row; y++) {
    //     for (let x = 0; x < this.width; x++) {
    //       const value = this.get(x, y)
    //       clonedData[x][y+1] = value
    //     }
    //   }
    // }
    
    /*
    |_|_|_|
    |x|x|x| 1
    |_|x|_|
    |x|x|x| 3


    |_|_|_|
    |_|_|_| 1
    |_|x|_|
    |x|x|x| 3

    */
    this.data = clonedData
  }
}
